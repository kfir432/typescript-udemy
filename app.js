"use strict";
// let age: number;
// type Complex = { data: number[]; output: (all: boolean) => number[] };
var bankAccount = {
    money: 2000,
    deposit: function (value) {
        this.money += value;
    }
};
var myself = {
    name: "kfir",
    bankAccount: bankAccount,
    hobbies: ["Surfing", "Diving"]
};
myself.bankAccount.deposit(3000);
console.log(myself);
