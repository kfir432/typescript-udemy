// let age: number;
// type Complex = { data: number[]; output: (all: boolean) => number[] };

// let complex2: Complex = {
//   data: [100, 10, 30],
//   output: function(all: boolean): number[] {
//     return this.data;
//   }
// };
// console.log(complex2.output);
type BankAccount = { money: number; deposit: (value: number) => void };
let bankAccount: BankAccount = {
  money: 2000,
  deposit(value: number): void {
    this.money += value;
  }
};

let myself: { name: string; bankAccount: BankAccount; hobbies: string[] } = {
  name: "kfir",
  bankAccount: bankAccount,
  hobbies: ["Surfing", "Diving"]
};

myself.bankAccount.deposit(3000);
console.log(myself);
